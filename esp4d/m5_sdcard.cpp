#include "config.h"
#ifdef USE_M5STACK

/*
 * Connect the SD card to the following pins:
 *
 * SD Card | ESP32
 *    D2       -
 *    D3       SS
 *    CMD      MOSI
 *    VSS      GND
 *    VDD      3.3V
 *    CLK      SCK
 *    VSS      GND
 *    D0       MISO
 *    D1       -
 */

//Ver: https://github.com/espressif/arduino-esp32/blob/a59eafbc9dfa3ce818c110f996eebf68d755be24/libraries/SD/README.md


#include "FS.h"
#include "SD.h"
#include "SPI.h"

#define M5_SDCARD_CS_PIN 4

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    ESP_SERIAL_PORT.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        ESP_SERIAL_PORT.println("Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        ESP_SERIAL_PORT.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            ESP_SERIAL_PORT.print("  DIR : ");
            ESP_SERIAL_PORT.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            ESP_SERIAL_PORT.print("  FILE: ");
            ESP_SERIAL_PORT.print(file.name());
            ESP_SERIAL_PORT.print("  SIZE: ");
            ESP_SERIAL_PORT.println(file.size());
        }
        file = root.openNextFile();
    }
}

void createDir(fs::FS &fs, const char * path){
    ESP_SERIAL_PORT.printf("Creating Dir: %s\n", path);
    if(fs.mkdir(path)){
        ESP_SERIAL_PORT.println("Dir created");
    } else {
        ESP_SERIAL_PORT.println("mkdir failed");
    }
}

void removeDir(fs::FS &fs, const char * path){
    ESP_SERIAL_PORT.printf("Removing Dir: %s\n", path);
    if(fs.rmdir(path)){
        ESP_SERIAL_PORT.println("Dir removed");
    } else {
        ESP_SERIAL_PORT.println("rmdir failed");
    }
}

void readFile(fs::FS &fs, const char * path){
    ESP_SERIAL_PORT.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        ESP_SERIAL_PORT.println("Failed to open file for reading");
        return;
    }

    ESP_SERIAL_PORT.print("Read from file: ");
    while(file.available()){
        ESP_SERIAL_PORT.write(file.read());
    }
    file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
    ESP_SERIAL_PORT.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        ESP_SERIAL_PORT.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        ESP_SERIAL_PORT.println("File written");
    } else {
        ESP_SERIAL_PORT.println("Write failed");
    }
    file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message){
    ESP_SERIAL_PORT.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        ESP_SERIAL_PORT.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
        ESP_SERIAL_PORT.println("Message appended");
    } else {
        ESP_SERIAL_PORT.println("Append failed");
    }
    file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
    ESP_SERIAL_PORT.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        ESP_SERIAL_PORT.println("File renamed");
    } else {
        ESP_SERIAL_PORT.println("Rename failed");
    }
}

void deleteFile(fs::FS &fs, const char * path){
    ESP_SERIAL_PORT.printf("Deleting file: %s\n", path);
    if(fs.remove(path)){
        ESP_SERIAL_PORT.println("File deleted");
    } else {
        ESP_SERIAL_PORT.println("Delete failed");
    }
}

void testFileIO(fs::FS &fs, const char * path){

		#define RW_BUFF_SIZE 8192
		#define BYTES (2*1024*1024)
    uint8_t* buf= (uint8_t*) malloc(RW_BUFF_SIZE);
		File file;
	  size_t len, file_size= BYTES;
		uint32_t ms, cuantos;

		file = fs.open(path);
    if (file) {
    	file.close();
    	deleteFile(SD, path);
    }

    ESP_SERIAL_PORT.printf("\r\n*** WRITING %s\r\n", path);
    ms= millis();
    file = fs.open(path, FILE_APPEND);
    if (!file) { ESP_SERIAL_PORT.print("WRITE ERROR\r\n"); goto fin; }
    cuantos= BYTES/RW_BUFF_SIZE;
    while (cuantos--) {
    	file.write(buf, RW_BUFF_SIZE);
    	if (cuantos%64) ESP_SERIAL_PORT.print("W"); else ESP_SERIAL_PORT.print("W\r\n");
    }
    file.close();
    ms= millis()- ms;
    ESP_SERIAL_PORT.print(ms);
    ESP_SERIAL_PORT.print("ms, FILE SIZE: ");
    ESP_SERIAL_PORT.print(file_size/1024);
    ESP_SERIAL_PORT.print(" kB\r\nWRITE SPEED: ");
    ESP_SERIAL_PORT.print(BYTES/ms);
    ESP_SERIAL_PORT.print(" kB/s\r\n");

    ESP_SERIAL_PORT.printf("\r\n*** READING %s\r\n", path);
    ms= millis();
    file = fs.open(path);
    if (!file) { ESP_SERIAL_PORT.print("READ ERROR\r\n"); goto fin; }
    file_size= file.size();
		len= file_size;
		cuantos= 0;
		while (len) {
				size_t toRead= len;
				if (toRead > RW_BUFF_SIZE) toRead= RW_BUFF_SIZE;
				file.read(buf, toRead);
				len-= toRead;
				if (++cuantos%64) ESP_SERIAL_PORT.print("R"); else ESP_SERIAL_PORT.print("R\r\n");
		}
		file.close();
    ms= millis()- ms;
    ESP_SERIAL_PORT.print(ms);
    ESP_SERIAL_PORT.print("ms, FILE SIZE: ");
    ESP_SERIAL_PORT.print(file_size/1024);
    ESP_SERIAL_PORT.print(" kB\r\nREAD SPEED: ");
    ESP_SERIAL_PORT.print(BYTES/ms);
    ESP_SERIAL_PORT.print(" kB/s\r\n\r\n");

    deleteFile(SD, path);
    ESP_SERIAL_PORT.print("\r\n");

fin:
		free(buf);
}

void m5_sdcard_setup () {

    if(!SD.begin(M5_SDCARD_CS_PIN)){
        ESP_SERIAL_PORT.println("Card Mount Failed");
        return;
    }
    uint8_t cardType = SD.cardType();

    if(cardType == CARD_NONE){
        ESP_SERIAL_PORT.println("No SD card attached");
        return;
    }

    ESP_SERIAL_PORT.print("\r\nSD Card Type: ");
    if(cardType == CARD_MMC){
        ESP_SERIAL_PORT.println("MMC");
    } else if(cardType == CARD_SD){
        ESP_SERIAL_PORT.println("SDSC");
    } else if(cardType == CARD_SDHC){
        ESP_SERIAL_PORT.println("SDHC");
    } else {
        ESP_SERIAL_PORT.println("UNKNOWN");
    }

    uint64_t cardSize = SD.cardSize() / (1024 * 1024);
    ESP_SERIAL_PORT.printf("SD Card Size: %llu MB\n", cardSize);
    ESP_SERIAL_PORT.printf("Total space: %llu MB\n", SD.totalBytes() / (1024 * 1024));
    ESP_SERIAL_PORT.ESP_SERIAL_PORT.printf("Used space: %llu MB\n", SD.usedBytes() / (1024 * 1024));
/*
    listDir(SD, "/", 0);
    createDir(SD, "/mydir");
    listDir(SD, "/", 0);
    removeDir(SD, "/mydir");
    listDir(SD, "/", 2);
    writeFile(SD, "/hello.txt", "Hello ");
    appendFile(SD, "/hello.txt", "World!\n");
    readFile(SD, "/hello.txt");
    deleteFile(SD, "/foo.txt");
    renameFile(SD, "/hello.txt", "/foo.txt");
    readFile(SD, "/foo.txt");
*/

    listDir(SD, "/", 0);
    testFileIO(SD, "/iotest.txt");
}

#endif
