#include "config.h"
#ifdef USE_TTGO_TDISPLAY

#include <TFT_eSPI.h>
#include <SPI.h>
#include "WiFi.h"
#include <Wire.h>
#include <Button2.h>
#include "esp_adc_cal.h"
#include "splash_scrn.h"
#include "config.h"

#ifndef TFT_DISPOFF
#define TFT_DISPOFF 0x28
#endif

#ifndef TFT_SLPIN
#define TFT_SLPIN   0x10
#endif

#define TFT_MOSI            19
#define TFT_SCLK            18
#define TFT_CS              5
#define TFT_DC              16
#define TFT_RST             23

#define TFT_BL          4   // Display backlight control pin
#define ADC_EN          14  //ADC_EN is the ADC detection enable port
#define ADC_PIN         34
#define BUTTON_1        35
#define BUTTON_2        0

TFT_eSPI tft= TFT_eSPI(135, 240); // Invoke custom library
Button2 btn1(BUTTON_1);
Button2 btn2(BUTTON_2);

int vref = 1100;
int btnCick = false;

//! Long time delay, it is recommended to use shallow sleep, which can effectively reduce the current consumption
void espDelay(int ms)
{
    esp_sleep_enable_timer_wakeup(ms * 1000);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_ON);
    esp_light_sleep_start();
}

void showVoltage()
{
    static uint64_t timeStamp = 0;
    if (millis() - timeStamp > 1000) {
        timeStamp = millis();
        uint16_t v = analogRead(ADC_PIN);
        float battery_voltage = ((float)v / 4095.0) * 2.0 * 3.3 * (vref / 1000.0);
        String voltage = "Voltage :" + String(battery_voltage) + "V";
        tft.fillScreen(TFT_BLACK);
        tft.setTextDatum(MC_DATUM);
        tft.drawString(voltage,  tft.width() / 2, tft.height() / 2 );
    }
}

void wifi_scan()
{
    char buff[512];
    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.fillScreen(TFT_BLACK);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(1);

    tft.drawString("Scan Network", tft.width() / 2, tft.height() / 2);

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);

    int16_t n = WiFi.scanNetworks();
    tft.fillScreen(TFT_BLACK);
    if (n == 0) {
        tft.drawString("no networks found", tft.width() / 2, tft.height() / 2);
    } else {
        tft.setTextDatum(TL_DATUM);
        tft.setCursor(0, 0);
        for (int i = 0; i < n; ++i) {
            sprintf(buff,
                    "[%d]:%s(%d)",
                    i + 1,
                    WiFi.SSID(i).c_str(),
                    WiFi.RSSI(i));
            tft.println(buff);
        }
    }
    WiFi.mode(WIFI_OFF);
}

int btn_released= 0;
void button_init()
{
    btn1.setLongClickHandler([](Button2 & b) {
        btnCick = false;
        int r = digitalRead(TFT_BL);
        tft.fillScreen(TFT_BLACK);
        tft.setTextColor(TFT_GREEN, TFT_BLACK);
        tft.setTextDatum(MC_DATUM);
        tft.drawString("Press again to wake up",  tft.width() / 2, tft.height() / 2 );
        espDelay(6000);
        digitalWrite(TFT_BL, !r);

        tft.writecommand(TFT_DISPOFF);
        tft.writecommand(TFT_SLPIN);
        //After using light sleep, you need to disable timer wake, because here use external IO port to wake up
        esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_TIMER);
        // esp_sleep_enable_ext1_wakeup(GPIO_SEL_35, ESP_EXT1_WAKEUP_ALL_LOW);
        esp_sleep_enable_ext0_wakeup(GPIO_NUM_35, 0);
        delay(200);
        esp_deep_sleep_start();
    });
    btn1.setPressedHandler([](Button2 & b) {
        btnCick = true;
    });

    btn2.setPressedHandler([](Button2 & b) {

      //Activar bridge a 115200 para firmware updates del ATMEGA
      tft.fillScreen(TFT_BLACK);
      tft.setTextColor(TFT_GREEN, TFT_BLACK);
      tft.setTextSize(2);
      tft.setTextDatum(MC_DATUM);
      tft.drawString("FIRMWARE UPDATE",  tft.width() / 2, tft.height() * 1 / 6 );
      tft.drawString("USB-ATMEGA BRIDGE",  tft.width() / 2, tft.height() * 3 /6 );
      tft.drawString("Use 115200 baud",  tft.width() / 2, tft.height() * 5 / 6);

      WiFi.mode(WIFI_OFF);
      PRINTER_CPU_SERIAL_PORT.print("\r\n\r\nM117 FW UPDATE MODE\r\n");
      PRINTER_CPU_SERIAL_PORT.flush();
      delay(234);
      CONFIG::InitBaudrate(115200);

      while (1) {

        //If() ¿?
        while (PRINTER_USB_SERIAL_PORT.available()) {
          PRINTER_CPU_SERIAL_PORT.write(PRINTER_USB_SERIAL_PORT.read());
        }

        //If() ¿?
        while (PRINTER_CPU_SERIAL_PORT.available()) {
          PRINTER_USB_SERIAL_PORT.write(PRINTER_CPU_SERIAL_PORT.read());
        }

        if (!btn_released) btn_released= digitalRead(BUTTON_2);
        else if (!digitalRead(BUTTON_2)) esp_restart();
      }
    });
}

void button_loop()
{
    btn1.loop();
    btn2.loop();
}

void board_setup()
{

    //ADC_EN is the ADC detection enable port
    //If the USB port is used for power supply, it is turned on by default.
    //If it is powered by battery, it needs to be set to high level

    pinMode(ADC_EN, OUTPUT);
    digitalWrite(ADC_EN, HIGH);

    ESP_SERIAL_PORT.begin (115200);
    ESP_SERIAL_PORT.setRxBufferSize(SERIAL_RX_BUFFER_SIZE);

    tft.init();
    tft.setRotation(1);
    tft.fillScreen(TFT_BLACK);
    tft.setTextSize(2);
    tft.setTextColor(TFT_GREEN);
    tft.setCursor(0, 0);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(1);

    if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
        pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
        digitalWrite(TFT_BL, TFT_BACKLIGHT_ON); // Turn backlight on. TFT_BACKLIGHT_ON has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
    }

    tft.setSwapBytes(true);
    tft.setRotation(3);
    tft.pushImage(0, 0,  240, 135, splash_scrn);

    button_init();

    esp_adc_cal_characteristics_t adc_chars;
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize((adc_unit_t)ADC_UNIT_1, (adc_atten_t)ADC1_CHANNEL_6, (adc_bits_width_t)ADC_WIDTH_BIT_12, 1100, &adc_chars);
    //Check type of calibration value used to characterize ADC
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        ESP_SERIAL_PORT.printf("eFuse Vref:%u mV", adc_chars.vref);
        vref = adc_chars.vref;
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        ESP_SERIAL_PORT.printf("Two Point --> coeff_a:%umV coeff_b:%umV\n", adc_chars.coeff_a, adc_chars.coeff_b);
    } else {
        ESP_SERIAL_PORT.println("Default Vref: 1100mV");
    }

    //tft.fillScreen(TFT_BLACK);
    //tft.setTextDatum(MC_DATUM);
//     tft.drawString("LeftButton:", tft.width() / 2, tft.height() / 2 - 16);
//     tft.drawString("[WiFi Scan]", tft.width() / 2, tft.height() / 2 );
//     tft.drawString("RightButton:", tft.width() / 2, tft.height() / 2 + 16);
//     tft.drawString("[Voltage Monitor]", tft.width() / 2, tft.height() / 2 + 32 );
//     tft.drawString("RightButtonLongPress:", tft.width() / 2, tft.height() / 2 + 48);
//     tft.drawString("[Deep Sleep]", tft.width() / 2, tft.height() / 2 + 64 );
//     tft.setTextDatum(TL_DATUM);

}

uint8_t flip_flop= 0;
uint32_t last_flip= 0;
void flip_screen_color (uint32_t ms) {
    if (millis() > (last_flip+ms)) {
        if (flip_flop) tft.fillScreen(TFT_BLACK);
        else tft.fillScreen(TFT_RED);
        flip_flop= !flip_flop;
        last_flip= millis();
    }
}

void board_loop()
{
    if (btnCick) {
        showVoltage();
    }
    button_loop();
    flip_screen_color(444);
}

#endif
