/*
  webinterface.cpp - ESP4D configuration class

  Copyright (c) 2014 Luc Lebosse. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <pgmspace.h>
#include "config.h"
#include "webinterface.h"
#include "wificonf.h"
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>
#include <StreamString.h>
#ifndef FS_NO_GLOBALS
#define FS_NO_GLOBALS
#endif
#include <FS.h>
#include <WiFi.h>
#include <WebServer.h>
#include "SPIFFS.h"
#include "Update.h"

#include "GenLinkedList.h"
#include "command.h"
#include "espcom.h"

#ifdef SSDP_FEATURE
#include <ESP32SSDP.h>
#endif

#include "syncwebserver.h"

#define MAX_AUTH_IP 10

long id_connection = 0;

uint8_t Checksum(const char * line, uint16_t lineSize)
{
    uint8_t checksum_val =0;
    for (uint16_t i=0; i < lineSize; i++) {
        checksum_val = checksum_val ^ ((uint8_t)line[i]);
    }
    return checksum_val;
}

String CheckSumLine(const char* line, uint32_t linenb)
{
    String linechecksum = "N" + String(linenb)+ " " + line;
    uint8_t crc = Checksum(linechecksum.c_str(), linechecksum.length());
    linechecksum+="*"+String(crc);
    return linechecksum;
}

bool purge_serial()
{
    ESPCOM::flush (DEFAULT_PRINTER_PIPE);
    uint32_t timeout= millis()+ 333;
    while (timeout>millis()) {
        while (ESPCOM::available(DEFAULT_PRINTER_PIPE)) PRINTER_CPU_SERIAL_PORT.read();
        delay(1);
    }
    LOG("Purge done\r\n")
    return true;
}

size_t wait_for_data(uint32_t timeout)
{
    uint32_t start = millis();
    while ((ESPCOM::available(DEFAULT_PRINTER_PIPE) < 3) || ((millis()-start) < timeout)) ;
    return ESPCOM::available(DEFAULT_PRINTER_PIPE);
}

uint32_t Get_lineNumber(String & response)
{
    int32_t l = 0;
    String sresend = "Resend:";
    if ( CONFIG::GetFirmwareTarget() == SMOOTHIEWARE) {
        sresend = "rs N";
    }
    int pos = response.indexOf(sresend);
    if (pos == -1 ) {
        return -1;
    }
    pos+=sresend.length();
    int pos2 = response.indexOf("\n", pos);
    String snum = response.substring(pos, pos2);
    //remove potential unwished char
    snum.replace("\r", "");
    l = snum.toInt();
    LOG("Line requested is ")
    LOG(String(l))
    LOG("\r\n")
    return l;
}



//Este número es crítico, un poco de más de la cuenta aquí,
//Y el ATMEGA perderá caracteres y entonces todo se va a la M.
#define MAX_BYTES_IN_FLIGHT 512
#define MAX_LINES_IN_FLIGHT 8
//El t máximo que estamos dispuestos a esperar a los OKs pendientes.
#define TIMEOUT_MS 3210
static int got_o= 0;
static int bytes_in_flight= 0;
static int lines_in_flight= 0;
static GenLinkedList<int> lengths_list;

void resetSendLine2Serial4Upload () {
//     LOG ( "resetSendLine2Serial4Upload()\r\n" )
    got_o= 0;
    bytes_in_flight= 0;
    lines_in_flight= 0;
    while (lengths_list.size()) lengths_list.remove(0);
}

bool SendLine2Serial4UploadError () {

//    LOG ( "SendLine2Serial4UploadError()\r\n" )
//    LOG ( "Serial.available: " )
//    LOG ( String(PRINTER_CPU_SERIAL_PORT.available()) )
//    LOG ( "\r\n" )

    char c;
    String r;
    uint32_t timeout_t= millis()+ TIMEOUT_MS;
    //ESPCOM::flush(DEFAULT_PRINTER_PIPE);
    while (millis()<timeout_t) {
        if (PRINTER_CPU_SERIAL_PORT.available()) {
            c= (char) PRINTER_CPU_SERIAL_PORT.read();
            //LOG ( String(c) )
            switch (c) {

                case '\r' :
                    got_o= 0;
                    break;

                case '\n' :
                    got_o= 0;
                    break;

                case 'o' :
                    if (got_o) {
                        r= "*** ERROR: DOS 'o' SEGUIDAS.\r\n";
                        goto hay_error;
                    }
                    got_o= 1;
                    break;

                case 'k' :
                    if (!got_o) {
                      r= "*** ERROR: 'k' SIN 'o' PREVIA.\r\n";
                      goto hay_error;
                    }
                    got_o= 0;
                    lines_in_flight-= 1;
                    bytes_in_flight-= lengths_list.get(0);
                    lengths_list.remove(0);
  //                  LOG ( "GOT_OK: PENDIENTES: " )
  //                  LOG ( String(lines_in_flight) )
  //                  LOG ( "\r" )
                    if (!lines_in_flight) return false;
                    break;

                default :
                  r= "*** ERROR: UNEXPECTED TEXT.";
                  LOG ( String("GOT_O: ")+ String(got_o)+ ", C: '"+ String((int)c) + "'\r\n" )
                  if (c == 255) {
                    delay(5);
                    continue;
                  }
                  goto hay_error;
            }
        }
    }

    r= "*** ERROR: TIMEOUT.";

hay_error:
    LOG ( r+ "\r\n" )
    LOG ( String("GOT_O: ")+ String(got_o)+ ", C: '"+ String((int)c) + "'\r\n" )
    r= "RX BUFFER: '"+ c;
    while (PRINTER_CPU_SERIAL_PORT.available()) {
      r+= (char) PRINTER_CPU_SERIAL_PORT.read();
    }
    LOG ( r+ "'\r\n" )
    return true;
}

bool SendLine2Serial4Upload (String &  line, int32_t linenum) {
//    LOG ( "SendLine2Serial4Upload()\r\n" )


    String line2send;
    line2send = CheckSumLine(line.c_str(), linenum)+ "\n";
    int len= line2send.length();
    if (len>MAX_BYTES_IN_FLIGHT) {
      LOG ("*** ERROR: len>MAX_BYTES_IN_FLIGHT\r\n")
      goto error;
    }

    if (lines_in_flight>=MAX_LINES_IN_FLIGHT) {
        if (SendLine2Serial4UploadError()) goto error;
    }

    ESPCOM::print(line2send, DEFAULT_PRINTER_PIPE);
    //ESPCOM::flush(DEFAULT_PRINTER_PIPE);
    lengths_list.add(len);
    bytes_in_flight+= len;
    lines_in_flight+= 1;
//    LOG ( "SENT: ")
//    LOG ( String(lines_in_flight)+ "\r" )
    return true;

error:
    resetSendLine2Serial4Upload();
    return false;
}


bool sendCommand2Serial (String& cmd, uint32_t wait) {
    //PURGE;
    PRINTER_CPU_SERIAL_PORT.flush();
    while (PRINTER_CPU_SERIAL_PORT.available()) {
        PRINTER_CPU_SERIAL_PORT.read();
        if (!PRINTER_CPU_SERIAL_PORT.available()) delay(1);
    }
    LOG ( "*** CMD2S: '"+ cmd+ "': " )
    PRINTER_CPU_SERIAL_PORT.print(cmd+ "\n");
    PRINTER_CPU_SERIAL_PORT.flush(true);
    String r= "";
    uint32_t timeout_t= millis()+ wait;
    while (millis()<timeout_t) {
        while (PRINTER_CPU_SERIAL_PORT.available()) {
            r+= (char) PRINTER_CPU_SERIAL_PORT.read();
            if (r.indexOf("ok") != -1) {
                LOG ( r+ "\r\n" )
                return true;
            }
        }
        delay(1);
    }
    LOG ( "*** CMD2S ERROR: '"+ r+ "'\r\n" )
    return false;
}


//function to send line to serial///////////////////////////////////////
//if newlinenb is NULL no auto correction of line number in case of resend
bool sendLine2Serial (String &  line, int32_t linenb,  int32_t * newlinenb)
{
    LOG ("Send line ")
    LOG (line )
    LOG ("  NB:")
    LOG (String(linenb))
    LOG ("\r\n")
    String line2send;
    String sok = "ok";
    String sresend = "Resend:";
    if (newlinenb) {
        *newlinenb = linenb;
    }
    if (linenb != -1) line2send = CheckSumLine(line.c_str(),linenb);
    else line2send = line;

    //purge serial as nothing is supposed to interfere with upload
    purge_serial();
    LOG ("Send line ")
    LOG (line2send )
    LOG ("\r\n")
    //send line
    ESPCOM::println (line2send, DEFAULT_PRINTER_PIPE);
    ESPCOM::flush(DEFAULT_PRINTER_PIPE);
    //check answer
    if (wait_for_data(999) > 0 ) {
        bool done = false;
        uint32_t timeout = millis();
        uint8_t count = 0;
        //got data check content
        String response ;
        while (!done) {
            size_t len = ESPCOM::available(DEFAULT_PRINTER_PIPE);
            //get size of buffer
            if (len > 0) {
                uint8_t * sbuf = (uint8_t *)malloc(len+1);
                if(!sbuf) {
                    return false;
                }
                //read buffer
                ESPCOM::readBytes (DEFAULT_PRINTER_PIPE, sbuf, len);
                //convert buffer to zero end array
                sbuf[len] = '\0';
                //use string because easier to handle and allow to re-assemble cutted answer
                response += (const char*) sbuf;
                LOG ("Response:\r\n************\r\n")
                LOG (response)
                LOG ("\r\n************\r\n")
                //in that case there is no way to know what is the right number to use and so send should be failed
                if (( ( CONFIG::GetFirmwareTarget() == REPETIER4DV) || (CONFIG::GetFirmwareTarget() == REPETIER) ) && (response.indexOf ("skip") != -1)) {
                    LOG ("Wrong line requested\r\n")
                    count = 5;
                }
                //it is resend ?
                int pos = response.indexOf (sresend);
                //be sure we get full line to be able to process properly
                if (( pos > -1) && (response.lastIndexOf("\n") > pos)) {
                    LOG ("Resend detected\r\n")
                    uint32_t line_number = Get_lineNumber(response);
                    //this part is only if have newlinenb variable
                    if (newlinenb != nullptr) {
                        *newlinenb = line_number;
                        free(sbuf);
                        //no need newlinenb in this one in theory, but just in case...
                        return sendLine2Serial (line, line_number, newlinenb);
                    } else {
                        //the line requested is not the current one so we stop
                        if (line_number !=linenb) {
                            LOG ("Wrong line requested\r\n")
                            count = 5;
                        }
                    }
                    count++;
                    if (count > 5) {
                        free(sbuf);
                        LOG ("Exit too many resend or wrong line\r\n")
                        return false;
                    }
                    purge_serial();
                    LOG ("Resend ")
                    LOG (String(count))
                    LOG ("\r\n")
                    response="";
                    ESPCOM::println (line2send, DEFAULT_PRINTER_PIPE);
                    ESPCOM::flush (DEFAULT_PRINTER_PIPE);
                    wait_for_data(1000);
                    timeout = millis();

                } else {
                    if ( (response.indexOf (sok) > -1) ) { //we have ok so it is done
                        free(sbuf);
                        LOG ("Got ok\r\n")
                        purge_serial();
                        return true;
                    }
                }
                free(sbuf);
            }
            //no answer or over buffer  exit
            if ( (millis() - timeout > 2000) ||  (response.length() >200)) {
                LOG ("Time out\r\n")
                done = true;
            }
            CONFIG::wait (1);
        }
    }
    LOG ("Send line error\r\n")
    return false;
}

//send M29 / M30 command to close file on SD////////////////////////////
void CloseSerialUpload (bool iserror, String & filename, int32_t linenb)
{
    String command = "M29";
    if (!sendCommand2Serial(command, 2345)) {
        String cmdfilename = "M30 " + filename;
        sendCommand2Serial (cmdfilename, 2345);
        ESPCOM::println (F ("SD upload failed"), PRINTER_PIPE);
        web_interface->_upload_status = UPLOAD_STATUS_FAILED;
    }
    else {
        ESPCOM::println (F ("SD upload done"), PRINTER_PIPE);
        web_interface->_upload_status = UPLOAD_STATUS_SUCCESSFUL;
    }
    //lets give time to FW to proceed
    //wait_for_data(333);
    //purge_serial();
    web_interface->blockserial = false;
}


//constructor
WEBINTERFACE_CLASS::WEBINTERFACE_CLASS (int port) : web_server (port)
{
    //that handle "/" and default index.html.gz
    web_server.on("/",HTTP_ANY, handle_web_interface_root);
    //need to be there even no authentication to say to UI no authentication
    web_server.on("/login", HTTP_ANY, handle_login);
    web_server.on("/favicon.ico", HTTP_ANY, handle_favicon);

    web_server.on("/api/version", HTTP_GET, handle_octoprint_version);
    web_server.on("/api/files/local", HTTP_POST, handle_return_200, handle_octoprint_upload);
    web_server.on("/api/job", HTTP_GET, handle_octoprint_api_job);
    web_server.on("/api/settings", HTTP_GET, handle_octoprint_api_settings);
    web_server.on("/api/printer", HTTP_GET, handle_octoprint_api_printer);
    web_server.on("/api/login", HTTP_POST, handle_octoprint_api_login);

#ifdef SSDP_FEATURE
    web_server.on ("/description.xml", HTTP_GET, handle_SSDP);
#endif
#ifdef CAPTIVE_PORTAL_FEATURE
    web_server.on("/generate_204",HTTP_ANY, handle_web_interface_root);
    web_server.on("/gconnectivitycheck.gstatic.com",HTTP_ANY, handle_web_interface_root);
    //do not forget the / at the end
    web_server.on("/fwlink/",HTTP_ANY, handle_web_interface_root);
#endif
    //SPIFFS
    web_server.on ("/files", HTTP_ANY, handleFileList, SPIFFSFileupload);
#ifdef WEB_UPDATE_FEATURE
    web_server.on ("/updatefw", HTTP_ANY, handleUpdate, WebUpdateUpload);
#endif
    //Page not found handler
    web_server.onNotFound ( handle_not_found);
    //web commands
    web_server.on ("/command", HTTP_ANY, handle_web_command);
    web_server.on ("/command_silent", HTTP_ANY, handle_web_command_silent);
    //Serial SD management
    web_server.on ("/upload_serial", HTTP_ANY, handle_serial_SDFileList, SDFile_serial_upload);

    blockserial = false;
    restartmodule = false;
    _head = NULL;
    _nb_ip = 0;
    _upload_status = UPLOAD_STATUS_NONE;
}
//Destructor
WEBINTERFACE_CLASS::~WEBINTERFACE_CLASS()
{
    while (_head) {
        auth_ip * current = _head;
        _head = _head->_next;
        delete current;
    }
    _nb_ip = 0;
}
//check authentification
level_authenticate_type  WEBINTERFACE_CLASS::is_authenticated()
{
#ifdef AUTHENTICATION_FEATURE
    if (web_server.hasHeader ("Cookie") ) {
        String cookie = web_server.header ("Cookie");
        int pos = cookie.indexOf ("ESPSESSIONID=");
        if (pos != -1) {
            int pos2 = cookie.indexOf (";", pos);
            String sessionID = cookie.substring (pos + strlen ("ESPSESSIONID="), pos2);
            IPAddress ip = web_server.client().remoteIP();
            //check if cookie can be reset and clean table in same time
            return ResetAuthIP (ip, sessionID.c_str() );
        }
    }
    return LEVEL_GUEST;
#else
    return LEVEL_ADMIN;
#endif
}

#ifdef AUTHENTICATION_FEATURE
//add the information in the linked list if possible
bool WEBINTERFACE_CLASS::AddAuthIP (auth_ip * item)
{
    if (_nb_ip > MAX_AUTH_IP) {
        return false;
    }
    item->_next = _head;
    _head = item;
    _nb_ip++;
    return true;
}

//Session ID based on IP and time using 16 char
char * WEBINTERFACE_CLASS::create_session_ID()
{
    static char  sessionID[17];
//reset SESSIONID
    for (int i = 0; i < 17; i++) {
        sessionID[i] = '\0';
    }
//get time
    uint32_t now = millis();
//get remote IP
    IPAddress remoteIP = web_server.client().remoteIP();
//generate SESSIONID
    if (0 > sprintf (sessionID, "%02X%02X%02X%02X%02X%02X%02X%02X", remoteIP[0], remoteIP[1], remoteIP[2], remoteIP[3], (uint8_t) ( (now >> 0) & 0xff), (uint8_t) ( (now >> 8) & 0xff), (uint8_t) ( (now >> 16) & 0xff), (uint8_t) ( (now >> 24) & 0xff) ) ) {
        strcpy (sessionID, "NONE");
    }
    return sessionID;
}



bool WEBINTERFACE_CLASS::ClearAuthIP (IPAddress ip, const char * sessionID)
{
    auth_ip * current = _head;
    auth_ip * previous = NULL;
    bool done = false;
    while (current) {
        if ( (ip == current->ip) && (strcmp (sessionID, current->sessionID) == 0) ) {
            //remove
            done = true;
            if (current == _head) {
                _head = current->_next;
                _nb_ip--;
                delete current;
                current = _head;
            } else {
                previous->_next = current->_next;
                _nb_ip--;
                delete current;
                current = previous->_next;
            }
        } else {
            previous = current;
            current = current->_next;
        }
    }
    return done;
}

//Get info
auth_ip * WEBINTERFACE_CLASS::GetAuth (IPAddress ip, const char * sessionID)
{
    auth_ip * current = _head;
    //auth_ip * previous = NULL;
    //get time
    //uint32_t now = millis();
    while (current) {
        if (ip == current->ip) {
            if (strcmp (sessionID, current->sessionID) == 0) {
                //found
                return current;
            }
        }
        //previous = current;
        current = current->_next;
    }
    return NULL;
}

//Review all IP to reset timers
level_authenticate_type WEBINTERFACE_CLASS::ResetAuthIP (IPAddress ip, const char * sessionID)
{
    auth_ip * current = _head;
    auth_ip * previous = NULL;
    //get time
    //uint32_t now = millis();
    while (current) {
        if ( (millis() - current->last_time) > 180000) {
            //remove
            if (current == _head) {
                _head = current->_next;
                _nb_ip--;
                delete current;
                current = _head;
            } else {
                previous->_next = current->_next;
                _nb_ip--;
                delete current;
                current = previous->_next;
            }
        } else {
            if (ip == current->ip) {
                if (strcmp (sessionID, current->sessionID) == 0) {
                    //reset time
                    current->last_time = millis();
                    return (level_authenticate_type) current->level;
                }
            }
            previous = current;
            current = current->_next;
        }
    }
    return LEVEL_GUEST;
}
#endif

//Check what is the content tye according extension file
String WEBINTERFACE_CLASS::getContentType (String filename)
{
    if (filename.endsWith (".htm") ) {
        return "text/html";
    } else if (filename.endsWith (".html") ) {
        return "text/html";
    } else if (filename.endsWith (".css") ) {
        return "text/css";
    } else if (filename.endsWith (".js") ) {
        return "application/javascript";
    } else if (filename.endsWith (".png") ) {
        return "image/png";
    } else if (filename.endsWith (".gif") ) {
        return "image/gif";
    } else if (filename.endsWith (".jpeg") ) {
        return "image/jpeg";
    } else if (filename.endsWith (".jpg") ) {
        return "image/jpeg";
    } else if (filename.endsWith (".ico") ) {
        return "image/x-icon";
    } else if (filename.endsWith (".xml") ) {
        return "text/xml";
    } else if (filename.endsWith (".pdf") ) {
        return "application/x-pdf";
    } else if (filename.endsWith (".zip") ) {
        return "application/x-zip";
    } else if (filename.endsWith (".gz") ) {
        return "application/x-gzip";
    } else if (filename.endsWith (".txt") ) {
        return "text/plain";
    }
    return "application/octet-stream";
}


WEBINTERFACE_CLASS * web_interface;
