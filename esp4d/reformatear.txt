Si el esp4d al arrancar ve que hay un fichero con el nombre "reformatear.txt"
en la partición SPIFFS, la reformateará. Cuando la velocidad del SPIFFS sea
exageradamente lenta (30kB/s o por ahí), seguramente un reformateo lo solucione.
Una velocidad "normal" de escritura es 50..100kB/s.
