/*
  ESP4D

  Copyright (c) 2014 Luc Lebosse. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//library include
#include "esp4d.h"
#include "config.h"

#include <ESPmDNS.h>
#include "m5stack.h"
#include "ttgo.h"
#include "iotest.h"

#ifdef USE_ARDUINO_OTA
#include <ArduinoOTA.h>
#endif


//global variables
Esp4D myesp4d;

#ifdef USE_BLUETOOTH_SERIAL
#include "BluetoothSerial.h"
BluetoothSerial SerialBT;
#endif


//Setup
void setup ()
{

    board_setup();
    iotest_spiffs(256*1024);

    myesp4d.begin();
    ESP_SERIAL_PORT.println(WiFi.getHostname());

#ifdef USE_ARDUINO_OTA
    ArduinoOTA.setHostname(WiFi.getHostname());
    ArduinoOTA.begin();
#endif

#ifdef USE_BLUETOOTH_SERIAL
    SerialBT.begin(WiFi.getHostname());
#endif

#ifdef MDNS_FEATURE
    MDNS.enableArduino();
    //MDNS.enableWorkstation();
    MDNS.addService("workstation", "tcp", 9);
    MDNS.addService("afpovertcp", "tcp", 548);
    MDNS.addService("utimaker", "tcp", 80);
    MDNS.addService("octoprint", "tcp", 80);
    MDNS.addService("telnet", "tcp", 8889);
#endif

/*
    MDNS.addService("octoprint", "tcp", 80);
    MDNS.setInstanceName("OctoPrint instance on ESP8266");
    MDNS.addServiceTxt("octoprint", "tcp", "api", "0.1");
    MDNS.addServiceTxt("octoprint", "tcp", "path", "/");
    MDNS.addServiceTxt("octoprint", "tcp", "version", "1.2.10");

    MDNS.addServiceTxt("ultimaker", "tcp", "api", "0.1");
    MDNS.addServiceTxt("ultimaker", "tcp", "path", "/");
    MDNS.addServiceTxt("ultimaker", "tcp", "version", "1.2.10");
*/

}


//main loop
void loop ()
{
    board_loop();
    myesp4d.process();
#ifdef USE_ARDUINO_OTA
    ArduinoOTA.handle();
#endif
}
