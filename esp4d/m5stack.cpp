#include "config.h"
#ifdef USE_M5STACK

#include <TFT_eSPI.h>
#include <SPI.h>
#include "WiFi.h"
#include <Wire.h>
#include <Button2.h>
#include "esp_adc_cal.h"
#include "splash_scrn.h"
#include "config.h"

#define BUTTON_3        37
#define BUTTON_2        38
#define BUTTON_1        39

TFT_eSPI tft = TFT_eSPI(240, 320); // Invoke custom library
Button2 btn1(BUTTON_1);
Button2 btn2(BUTTON_2);
Button2 btn3(BUTTON_3);

//! Long time delay, it is recommended to use shallow sleep, which can effectively reduce the current consumption
void espDelay(int ms)
{
    esp_sleep_enable_timer_wakeup(ms * 1000);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_ON);
    esp_light_sleep_start();
}

void btn1_onclick()
{
    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.fillScreen(TFT_BLACK);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(1);

    tft.drawString("1: Hola!", tft.width() / 2, tft.height() / 2);
    delay(1234);

}

void btn2_onclick()
{
    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.fillScreen(TFT_BLACK);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(2);

    tft.drawString("2: Hola!", tft.width() / 2, tft.height() / 2);
    delay(1234);

}

void btn3_onclick()
{
    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.fillScreen(TFT_BLACK);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(3);

    tft.drawString("3: Hola!", tft.width() / 2, tft.height() / 2);
    delay(1234);

}

void button_init()
{
    btn1.setLongClickHandler([](Button2 & b) {

    });

    btn1.setPressedHandler([](Button2 & b) {
        btn1_onclick();
    });

    btn2.setPressedHandler([](Button2 & b) {
        btn2_onclick();
    });

    btn3.setPressedHandler([](Button2 & b) {
        btn3_onclick();
    });
}

void button_loop()
{
    btn1.loop();
    btn2.loop();
    btn3.loop();
}

void board_setup()
{

		ESP_SERIAL_PORT.begin (115200);
		ESP_SERIAL_PORT.setRxBufferSize(SERIAL_RX_BUFFER_SIZE);


    tft.init();
    tft.setRotation(1);
    tft.fillScreen(TFT_BLACK);
    tft.setTextSize(2);
    tft.setTextColor(TFT_GREEN);
    tft.setCursor(0, 0);
    tft.setTextDatum(MC_DATUM);
    tft.setTextSize(1);

    if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
        pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
        digitalWrite(TFT_BL, TFT_BL); // Turn backlight on. TFT_BACKLIGHT_ON has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
    }

    tft.setSwapBytes(true);
    tft.pushImage(40, 52,  240, 135, splash_scrn);
    espDelay(1234);

    button_init();

}

uint32_t flip_ms= 0;
uint8_t flip_flop= 1;
void board_loop()
{
    button_loop();
    if (millis() > flip_ms) {
				tft.fillScreen(flip_flop ? TFT_BLACK : TFT_GREEN);
				tft.setRotation(1);
				tft.setSwapBytes(true);
				tft.pushImage(40, 52,  240, 135, splash_scrn);
    		flip_ms= millis()+ 1234;
    		flip_flop= !flip_flop;
    }
}

#endif
