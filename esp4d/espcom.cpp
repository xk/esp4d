/*
  espcom.cpp - esp4d communication serial/tcp/etc.. class

  Copyright (c) 2014 Luc Lebosse. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "config.h"
#include "espcom.h"
#include "command.h"
#include "webinterface.h"
#include "syncwebserver.h"

#ifdef ESP_OLED_FEATURE
#include "esp_oled.h"
bool  ESPCOM::block_2_oled = false;
#endif

uint8_t  ESPCOM::current_socket_id=0;

#ifdef TCP_SERIAL_SERVER
WiFiServer * tcp_serial_server;
WiFiClient tcp_serial_clients[MAX_SRV_CLIENTS];
#endif

#ifdef TELNET_SERVER
WiFiServer * telnet_server;
WiFiClient telnet_client;
#endif

bool ESPCOM::block_2_printer = false;


void ESPCOM::bridge (bool async)
{
    if ((WiFi.getMode() != WIFI_OFF) || wifi_config.WiFi_on) {

#ifdef TCP_SERIAL_SERVER
        ESPCOM::processFromTCP2Serial();
#endif

#ifdef TELNET_SERVER
        ESPCOM::processTelnet();
#endif

    }

    ESPCOM::processFromSerial();
}


long ESPCOM::readBytes (tpipe output, uint8_t * sbuf, size_t len)
{
    switch (output) {
    case SERIAL_PIPE:
        return PRINTER_CPU_SERIAL_PORT.readBytes(sbuf,len);
        break;
    default:
        return 0;
        break;
    }
}


long ESPCOM::baudRate (tpipe output)
{
    long br = 0;
    switch (output) {
    case SERIAL_PIPE:
        br = PRINTER_CPU_SERIAL_PORT.baudRate();
        break;
    default:
        return 0;
        break;
    }
    //workaround for ESP32
    if (br == 115201) {
        br = 115200;
    }
    if (br == 230423) {
        br = 230400;
    }
    return br;
}


size_t ESPCOM::available (tpipe output)
{
    switch (output) {
    case SERIAL_PIPE:
        return PRINTER_CPU_SERIAL_PORT.available();
        break;
    default:
        return 0;
        break;
    }
}


size_t ESPCOM::write (tpipe output, uint8_t d)
{
    if ((DEFAULT_PRINTER_PIPE == output) && (block_2_printer || CONFIG::is_locked(FLAG_BLOCK_SERIAL))) {
        return 0;
    }
    if ((SERIAL_PIPE == output) && CONFIG::is_locked(FLAG_BLOCK_SERIAL)) {
        return 0;
    }
    switch (output) {
    case SERIAL_PIPE:
        return PRINTER_CPU_SERIAL_PORT.write(d);
        break;
    default:
        return 0;
        break;
    }
}


void ESPCOM::flush (tpipe output, ESPResponseStream  *espresponse)
{
    switch (output) {
    case SERIAL_PIPE:
        PRINTER_CPU_SERIAL_PORT.flush(true);
        break;
    case WEB_PIPE:
        if (espresponse) {
            if(espresponse->header_sent) {
                //send data
                web_interface->web_server.sendContent(espresponse->buffer_web);
                //close line
                web_interface->web_server.sendContent("");
            }
            espresponse->header_sent = false;
            espresponse->buffer_web = String();
        }
        break;
    default:
        break;

    }
}


void ESPCOM::print (const __FlashStringHelper *data, tpipe output, ESPResponseStream  *espresponse)
{
    String tmp = data;
    ESPCOM::print (tmp.c_str(), output, espresponse);
}


void ESPCOM::print (String & data, tpipe output, ESPResponseStream  *espresponse)
{
    ESPCOM::print (data.c_str(), output, espresponse);
}


void ESPCOM::print (const char * data, tpipe output, ESPResponseStream  *espresponse)
{
    if ((DEFAULT_PRINTER_PIPE == output) && ( block_2_printer || CONFIG::is_locked(FLAG_BLOCK_SERIAL))) {
        return;
    }
    if ((SERIAL_PIPE == output) && CONFIG::is_locked(FLAG_BLOCK_SERIAL)) {
        return;
    }
#ifdef TCP_SERIAL_SERVER
    if ((TCP_PIPE == output) && CONFIG::is_locked(FLAG_BLOCK_TCP)) {
        return;
    }
#endif
#ifdef WS_DATA_FEATURE
    if ((WS_PIPE == output) && CONFIG::is_locked(FLAG_BLOCK_WSOCKET)) {
        return;
    }
#endif
#ifdef ESP_OLED_FEATURE
    if ((OLED_PIPE == output) && CONFIG::is_locked(FLAG_BLOCK_OLED)) {
        return;
    }
#endif
    switch (output) {
    case SERIAL_PIPE:
        PRINTER_CPU_SERIAL_PORT.print(data);
        break;
#ifdef TCP_SERIAL_SERVER
    case TCP_PIPE:
        ESPCOM::send2TCP(data);
        break;
#endif
    case WEB_PIPE:
        if (espresponse != NULL) {
            if (!espresponse->header_sent) {
                web_interface->web_server.setContentLength(CONTENT_LENGTH_UNKNOWN);
                web_interface->web_server.sendHeader("Content-Type","text/html");
                web_interface->web_server.sendHeader("Cache-Control","no-cache");
                web_interface->web_server.send(200);
                espresponse->header_sent = true;
            }
            espresponse->buffer_web+=data;
            if (espresponse->buffer_web.length() > 1200) {
                //send data
                web_interface->web_server.sendContent(espresponse->buffer_web);
                //reset buffer
                espresponse->buffer_web="";
            }
        }
        break;
#ifdef WS_DATA_FEATURE
    case WS_PIPE: {
        websocket_server->sendBIN(current_socket_id,(const uint8_t *)data,strlen(data));
    }
    break;
#endif

#ifdef ESP_OLED_FEATURE
    case OLED_PIPE: {
        if (!ESPCOM::block_2_oled) {
            if(!(!strcmp(data,"\n")||!strcmp(data,"\r")||!strcmp(data,"\r\n"))) {
                OLED_DISPLAY::print(data);
                OLED_DISPLAY::update_lcd();
            }
        }
    }
    break;
#endif
    case PRINTER_PIPE: {
#ifdef ESP_OLED_FEATURE
        OLED_DISPLAY::setCursor(0, 48);
        if(!(!strcmp(data,"\n")||!strcmp(data,"\r")||!strcmp(data,"\r\n"))) {
            ESPCOM::print(data, OLED_PIPE);
        }
#endif
        if (!CONFIG::is_locked(FLAG_BLOCK_M117)) {
            if(!(!strcmp(data,"\n")||!strcmp(data,"\r")||!strcmp(data,"\r\n"))) {
                ESPCOM::print ("M117 ", DEFAULT_PRINTER_PIPE);
            }
            ESPCOM::print (data, DEFAULT_PRINTER_PIPE);
        }
    }
    break;
    default:
        break;
    }
}


void ESPCOM::println (const __FlashStringHelper *data, tpipe output, ESPResponseStream  *espresponse)
{
    ESPCOM::print (data, output, espresponse);
#ifdef TCP_SERIAL_SERVER
    ESPCOM::print ("\r", output, espresponse);
#endif
    ESPCOM::print ("\n", output, espresponse);
}


void ESPCOM::println (String & data, tpipe output, ESPResponseStream  *espresponse)
{
    ESPCOM::print (data, output, espresponse);
#ifdef TCP_SERIAL_SERVER
    ESPCOM::print ("\r", output, espresponse);
#endif
    ESPCOM::print ("\n", output, espresponse);
}


void ESPCOM::println (const char * data, tpipe output, ESPResponseStream  *espresponse)
{
    ESPCOM::print (data, output, espresponse);
#ifdef TCP_SERIAL_SERVER
    ESPCOM::print ("\r", output, espresponse);
#endif
    ESPCOM::print ("\n", output, espresponse);
}


#ifdef TCP_SERIAL_SERVER
void ESPCOM::send2TCP (const __FlashStringHelper *data, bool async)
{
    String tmp = data;
    ESPCOM::send2TCP (tmp.c_str(), async);
}


void ESPCOM::send2TCP (String data, bool async)
{
    ESPCOM::send2TCP (data.c_str(), async);
}


void ESPCOM::send2TCP (const char * data, bool async)
{
    if (!async) {
        for (uint8_t i = 0; i < MAX_SRV_CLIENTS; i++) {
            if (tcp_serial_clients[i] && tcp_serial_clients[i].connected() ) {
                tcp_serial_clients[i].write (data, strlen (data) );
                delay (0);
            }
        }
    }
}


void ESPCOM::processFromTCP2Serial ()
{
    uint8_t i, data;
    //check if there are any new clients
    if (tcp_serial_server->hasClient() ) {
        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
            //find free/disconnected spot
            if (!tcp_serial_clients[i] || !tcp_serial_clients[i].connected() ) {
                if (tcp_serial_clients[i]) {
                    tcp_serial_clients[i].stop();
                }
                tcp_serial_clients[i] = tcp_serial_server->available();
                continue;
            }
        }
        //no free/disconnected spot so reject
        WiFiClient serverClient = tcp_serial_server->available();
        serverClient.stop();
    }
    //check clients for data
    //to avoid any pollution if Uploading file to SDCard
    if (!((web_interface->blockserial)  || CONFIG::is_locked(FLAG_BLOCK_TCP) || CONFIG::is_locked(FLAG_BLOCK_SERIAL))) {
        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
            if (tcp_serial_clients[i] && tcp_serial_clients[i].connected() ) {
                if (tcp_serial_clients[i].available() ) {
                    //get data from the tcp client and push it to the UART
                    while (tcp_serial_clients[i].available() ) {
                        data = tcp_serial_clients[i].read();
                        ESPCOM::write(DEFAULT_PRINTER_PIPE, data);
                        COMMAND::read_buffer_tcp (data);
                    }
                }
            }
        }
    }
}
#endif


bool ESPCOM::processFromSerial (bool async)
{
    uint8_t i;
    //check UART for data
    if (ESPCOM::available(DEFAULT_PRINTER_PIPE)) {
        size_t len = ESPCOM::available(DEFAULT_PRINTER_PIPE);
        uint8_t * sbuf = (uint8_t *)malloc(len+1);
        if(!sbuf) {
            return false;
        }
        sbuf[len] = '\0';
        ESPCOM::readBytes (DEFAULT_PRINTER_PIPE, sbuf, len);
#ifdef TCP_SERIAL_SERVER
        if (!async &&  !CONFIG::is_locked(FLAG_BLOCK_TCP)) {
            if ((WiFi.getMode() != WIFI_OFF)  || !wifi_config.WiFi_on) {
                //push UART data to all connected tcp clients
                for (i = 0; i < MAX_SRV_CLIENTS; i++) {
                    if (tcp_serial_clients[i] && tcp_serial_clients[i].connected() ) {
                        tcp_serial_clients[i].write (sbuf, len);
                        delay (0);
                    }
                }
            }
        }
#endif
#ifdef WS_DATA_FEATURE

        if (!CONFIG::is_locked(FLAG_BLOCK_WSOCKET) && websocket_server) {
            websocket_server->sendBIN(current_socket_id,sbuf,len);
        }
#endif
        //process data if any
        COMMAND::read_buffer_serial (sbuf, len);
        free(sbuf);
        return true;
    } else {
        return false;
    }
}


#ifdef TELNET_SERVER
void ESPCOM::processTelnet()
{
    while (telnet_server->hasClient()) {
        if (!telnet_client.connected()) {
            telnet_client= telnet_server->available();
            telnet_client.print("Hola!\r\n");
        }
        else telnet_server->available().stop();
    }

    if (telnet_client.connected()) {
        String input= "";
        while (telnet_client.available()) {
            input+= (char)telnet_client.read();
        }
        if (input.length()) telnet_client.print("ok:"+ input);
    }
}
#endif
