#include "config.h"
#ifdef USE_TTGO_TDISPLAY

void board_setup();
void board_loop();
void flip_screen_color (uint32_t ms);

#endif
